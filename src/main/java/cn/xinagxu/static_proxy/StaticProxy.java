package cn.xinagxu.static_proxy;

/**
 * @author 王杨帅
 * @create 2018-08-19 20:26
 * @desc
 **/
public class StaticProxy implements ISservice {

    SomeService target;

    public StaticProxy(SomeService target) {
        this.target = target;
    }

    public String someMethod01() {
        String result = target.someMethod01();
        return result.toUpperCase();
    }

    public void someMethod02() {
        target.someMethod02();
    }
}

