package cn.xinagxu.static_proxy;

/**
 * @author 王杨帅
 * @create 2018-08-19 20:27
 * @desc
 **/
public interface ISservice {
    String someMethod01();
    void someMethod02();
}

