package cn.xinagxu.static_proxy;

/**
 * @author 王杨帅
 * @create 2018-08-19 20:32
 * @desc
 **/
public class StaticProxtTest {

    public static void main(String[] args) {

        SomeService target = new SomeService();

        StaticProxy proxy = new StaticProxy(target);

        String result = proxy.someMethod01();

        System.out.println(result);

        proxy.someMethod02();

    }

}

