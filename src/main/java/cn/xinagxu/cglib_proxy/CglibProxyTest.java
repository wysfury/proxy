package cn.xinagxu.cglib_proxy;

/**
 * @author 王杨帅
 * @create 2018-08-19 21:12
 * @desc
 **/
public class CglibProxyTest {

    public static void main(String[] args) {

        SomeService target = new SomeService();

        // 创建代理对象
        SomeService proxy = new CglibFactory(target).createProxy();

        System.out.println(proxy.someMethod01());

        proxy.someMethod02();

    }

}

