package cn.xinagxu.dynamic_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author 王杨帅
 * @create 2018-08-19 20:40
 * @desc
 **/
public class DynamicProxyTest {

    public static void main(String[] args) {

        final SomeService target = new SomeService();

        ISservice proxy = (ISservice) Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                        if ("someMethod01".equals(method.getName())) {
                            String str = (String) method.invoke(target, args);
                            return str.toUpperCase();
                        } else {
                            Object result = method.invoke(target, args);
                            return result;
                        }

                    }
                }
        );

        System.out.println(proxy.someMethod01());

        proxy.someMethod02();



    }

}

