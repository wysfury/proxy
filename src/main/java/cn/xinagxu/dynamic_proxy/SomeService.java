package cn.xinagxu.dynamic_proxy;

/**
 * @author 王杨帅
 * @create 2018-08-19 20:25
 * @desc
 **/
public class SomeService implements ISservice {

    public String someMethod01() {
        return "someMethod01";
    }

    public void someMethod02() {
        System.out.println("someMethod02");
    }

}

